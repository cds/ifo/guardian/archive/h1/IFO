# -*- mode: python; tab-width: 4 -*-
#
# IFO Guardian top node
#
# All states in this node are set request=False, which sets the node
# in "monitor only" mode.  In this case, the node ignores the REQUEST
# state when reporting status (ARRIVED, OK).

from guardian import GuardState, GuardStateDecorator
from guardian.manager import NodeManager

import IFO_NODE_LIST

##################################################

request = 'COMMISSION'
nominal = 'OBSERVE'
GRD_MANAGER = 'OPERATOR'

##################################################

# the NodeManager is given all nodes in the system
nodes = NodeManager(IFO_NODE_LIST.IFO_NODE_LIST)

##################################################

def node_fault():
    """Return True if any node in a fault condition.

    """
    any_fault = False
    for node in nodes:
        fault = node.check_fault()
        any_fault |= fault
    return any_fault

def all_nodes_ok():
    """Return True if all nodes are reporting OK.

    """
    not_ok = nodes.not_ok()
    for node in not_ok:
        notify("waiting for node: %s" % node)
    return len(not_ok) == 0

##################################################

class INIT(GuardState):
    index = 0
    request = False

    def main(self):
        log("nodes monitored:")
        for node in sorted(nodes, key=lambda node: node.name):
            log("  %s" % node.name)

    @nodes.checker('NODE_FAULT')
    def run(self):
        return True


class NODE_FAULT(GuardState):
    index = 10
    request = False

    def run(self):
        if not nodes.check_fault():
            return 'WAITING_FOR_NODES'
        return False


class WAITING_FOR_NODES(GuardState):
    index = 20
    request = False

    @nodes.checker('NODE_FAULT')
    def run(self):
        if all_nodes_ok():
            return 'READY'
        return False


class READY(GuardState):
    index = 50
    request = False

    @nodes.checker('NODE_FAULT')
    def run(self):
        if not all_nodes_ok():
            return 'WAITING_FOR_NODES'
        return True


class COMMISSION(GuardState):
    index = 90

    @nodes.checker('NODE_FAULT')
    def run(self):
        if not all_nodes_ok():
            return 'WAITING_FOR_NODES'
        return True


class OBSERVE(GuardState):
    index = 100

    def main(self):
        # FIXME: Is there a way around this self-referencing???
        # If theres no one here, set the obs mode to Observe (10)
        if ezca['GRD-IFO_MODE'] == 0:
            ezca['ODC-OBSERVATORY_MODE'] = 10

    @nodes.checker('NODE_FAULT')
    def run(self):
        if not all_nodes_ok():
            return 'DROP_OBSERVE'
        return True


class DROP_OBSERVE(GuardState):
    index = 5
    request = False

    def main(self):
        # FIXME: Is there a way around this self-referencing???
        # If theres no one here, set the obs mode to Lock Acq (21)
        if ezca['GRD-IFO_MODE'] == 0:
            ezca['ODC-OBSERVATORY_MODE'] = 21

    @nodes.checker('NODE_FAULT')
    def run(self):
        return 'WAITING_FOR_NODES'


##################################################

edges = [
    ('INIT', 'WAITING_FOR_NODES'),
    ('WAITING_FOR_NODES', 'READY'),
    ('READY', 'OBSERVE'),
    ('READY', 'COMMISSION'),
    ('COMMISSION', 'OBSERVE'),
    ('OBSERVE', 'COMMISSION'),
    ('NODE_FAULT', 'WAITING_FOR_NODES'),
    ('DROP_OBSERVE', 'WAITING_FOR_NODES'),
]
