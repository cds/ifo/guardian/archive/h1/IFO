# -*- mode: python; tab-width: 4 -*-
#
# LHO Guardian top node node list

##################################################

import subprocess


# NODES THAT SHOULD NOT BE MONITORED IN OBSERVING
EXCLUDE_NODES = [
    'BRSX_STAT',
    'BRSY_STAT',
    'DIAG_MAIN',
    'H1_MANAGER', # This node should get taken out of this list after testing
    'HIGH_FREQ_LINES',
    'IFO_NOTIFY',
    'IFO_UNDISTURBED',
    'PCALX_STAT',
    'PEM_MAG_INJ',
    'SEI_CONF',
    'SEI_DIFF',
    'SEI_ENV',
    'SEI_CS',
    'SQZ_SHG',
    'SQZ_OPO_LR',
    'SQZ_CLF_LR',
    'SQZ_LO_LR',
    'SQZ_FC',
    'SR3_CAGE_SERVO',
    'SUS_CHARGE',
    'TEST',
    'TEST_NOTIFY',
    ]


def get_nominal_nodes():
    all_nodes = subprocess.run(
        ['guardctrl', 'list'],
        capture_output=True,
        universal_newlines=True,
        check=True,
    ).stdout.split()
    exclude_nodes = ['IFO'] + EXCLUDE_NODES
    # also exclude SEI Config nodes
    conf_nodes = [conf for conf in all_nodes if '_SC' in conf or 'BLND' in conf]
    exclude_nodes += conf_nodes
    return list(set(all_nodes) - set(exclude_nodes))


IFO_NODE_LIST = get_nominal_nodes()


#############################################

if __name__ == '__main__':
    from ezca import Ezca
    ezca = Ezca()
    
    nodes = get_nominal_nodes()

    print('\nMonitored nodes list:\n')
    for node in nodes:
        ok = 'OK' if ezca['GRD-{}_OK'.format(node)] else 'NOT OK'
        print('{:<25}{}'.format(node, ok))
    
